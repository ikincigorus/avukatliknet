namespace avukatliknet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_SIKAYET_AVUKAT
    {
        public long id { get; set; }

        [StringLength(50)]
        public string sikayet_kodu { get; set; }

        public long? sikayet_id { get; set; }

        public long? avukat_id { get; set; }

        public string teklif_aciklama { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? teklif_tarihi { get; set; }

        public int? durumu { get; set; }

        public virtual T_AVUKAT T_AVUKAT { get; set; }

        public virtual T_SIKAYET T_SIKAYET { get; set; }
    }
}
