﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace avukatliknet.Models
{
    public class _site
    {
        public T_AVUKAT M_AVUKAT { get; set; }
        public List<T_AVUKAT> M_AVUKATS { get; set; }
        public T_AVUKAT_BRANS M_AVUKAT_BRANS { get; set; }
        public List<T_AVUKAT_BRANS> M_AVUKAT_BRANSS { get; set; }
        public T_AVUKAT_SEHIR M_AVUKAT_SEHIR { get; set; }
        public List<T_AVUKAT_SEHIR> M_AVUKAT_SEHIRS { get; set; }
        public T_BRANS M_BRANS { get; set; }
        public List<T_BRANS> M_BRANSS { get; set; }
        public T_PARAMS M_PARAMS { get; set; }
        public List<T_PARAMS> M_PARAMSS { get; set; }
        public T_SEHIR M_SEHIR { get; set; }
        public List<T_SEHIR> M_SEHIRS { get; set; }
        public T_SIKAYET M_SIKAYET { get; set; }
        public List<T_SIKAYET> M_SIKAYETS { get; set; }
        public T_SIKAYET_AVUKAT M_SIKAYET_AVUKAT { get; set; }
        public List<T_SIKAYET_AVUKAT> M_SIKAYET_AVUKATS { get; set; }
        public T_AVUKAT_KREDI_LOG M_KREDI_LOG { get; set; }
        public List<T_AVUKAT_KREDI_LOG> M_KREDI_LOGS { get; set; }
        public T_SLIDER_AVUKAT M_SLIDER_AVUKAT { get; set; }
        public List<T_SLIDER_AVUKAT> M_SLIDER_AVUKATS { get; set; }


        public DataTable ParamDataTable = new DataTable();

        public string DosyaStr { get; set; }
        public string BransStr { get; set; }
        public string ErrorStr { get; set; }
        public string SuccessStr { get; set; }
        public string KrediParams { get; set; }
        public string DosyaKod { get; set; }
        public string EPostaAdresi { get; set; }
        public int DosyaGoster { get; set; }
        public string DurumStr { get; set; }
        public string SehirLimit { get; set; }
        public string BransLimit { get; set; }
        public string TavsiyeAdSoyad { get; set; }
        public string TavsiyeRefKod { get; set; }
        public long AvukatSayi { get; set; }
        public long DosyaSayi { get; set; }
        public long MusteriSayi { get; set; }
    }
}