namespace avukatliknet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_AVUKAT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_AVUKAT()
        {
            T_AVUKAT_BRANS = new HashSet<T_AVUKAT_BRANS>();
            T_AVUKAT_DOSYA_ISLEM = new HashSet<T_AVUKAT_DOSYA_ISLEM>();
            T_AVUKAT_SEHIR = new HashSet<T_AVUKAT_SEHIR>();
            T_SIKAYET_AVUKAT = new HashSet<T_SIKAYET_AVUKAT>();
            T_SLIDER_AVUKAT = new HashSet<T_SLIDER_AVUKAT>();
            T_AVUKAT_KREDI_LOG = new HashSet<T_AVUKAT_KREDI_LOG>();
        }

        public long id { get; set; }

        [StringLength(40)]
        public string avukat_code { get; set; }

        [StringLength(50)]
        public string avukat_resim { get; set; }

        [StringLength(50)]
        public string adi_soyadi { get; set; }

        [StringLength(20)]
        public string cep_telefonu { get; set; }

        [StringLength(150)]
        public string e_posta_adresi { get; set; }

        [StringLength(100)]
        public string adresi { get; set; }

        public int? il_id { get; set; }

        [StringLength(50)]
        public string bagli_olunan_baro { get; set; }

        [StringLength(100)]
        public string sifre { get; set; }

        public string ozgecmis { get; set; }

        public long? kredi { get; set; }

        public long? goruntuleme_sayisi { get; set; }

        [StringLength(3)]
        public string bizi_nereden_duydunuz { get; set; }

        [StringLength(50)]
        public string avukat_oneren { get; set; }

        public long? aktiflik_puani { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? uyelik_tarihi { get; set; }

        public int? brans_ekleme_sayisi { get; set; }

        public int? sehir_ekleme_sayisi { get; set; }

        public int? durumu { get; set; }

        public int? yonetici { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_AVUKAT_BRANS> T_AVUKAT_BRANS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_AVUKAT_DOSYA_ISLEM> T_AVUKAT_DOSYA_ISLEM { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_AVUKAT_SEHIR> T_AVUKAT_SEHIR { get; set; }

        public virtual T_SEHIR T_SEHIR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_SIKAYET_AVUKAT> T_SIKAYET_AVUKAT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_SLIDER_AVUKAT> T_SLIDER_AVUKAT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_AVUKAT_KREDI_LOG> T_AVUKAT_KREDI_LOG { get; set; }
    }
}
