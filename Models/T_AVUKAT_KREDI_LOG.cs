namespace avukatliknet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_AVUKAT_KREDI_LOG
    {
        public long id { get; set; }

        public long? avukat_id { get; set; }

        [StringLength(15)]
        public string odeme_id { get; set; }

        public long? kredi_limit { get; set; }

        [StringLength(1)]
        public string islem { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? islem_tarihi { get; set; }

        [StringLength(500)]
        public string aciklama { get; set; }

        public virtual T_AVUKAT T_AVUKAT { get; set; }
    }
}
