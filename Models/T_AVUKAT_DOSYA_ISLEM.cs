namespace avukatliknet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_AVUKAT_DOSYA_ISLEM
    {
        public long id { get; set; }

        public long? avukat_id { get; set; }

        public long? sikayet_id { get; set; }

        public int? dosya_islem { get; set; }

        public int? dosya_islem_sayisi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? islem_tarih { get; set; }

        public virtual T_AVUKAT T_AVUKAT { get; set; }

        public virtual T_SIKAYET T_SIKAYET { get; set; }
    }
}
