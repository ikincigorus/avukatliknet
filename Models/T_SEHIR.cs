namespace avukatliknet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_SEHIR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_SEHIR()
        {
            T_AVUKAT = new HashSet<T_AVUKAT>();
            T_AVUKAT_SEHIR = new HashSet<T_AVUKAT_SEHIR>();
            T_SIKAYET = new HashSet<T_SIKAYET>();
        }

        public int id { get; set; }

        public int? il_id { get; set; }

        [StringLength(50)]
        public string il_adi { get; set; }

        public int? il_oncelik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_AVUKAT> T_AVUKAT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_AVUKAT_SEHIR> T_AVUKAT_SEHIR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_SIKAYET> T_SIKAYET { get; set; }
    }
}
