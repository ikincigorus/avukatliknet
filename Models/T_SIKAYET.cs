namespace avukatliknet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_SIKAYET
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_SIKAYET()
        {
            T_AVUKAT_DOSYA_ISLEM = new HashSet<T_AVUKAT_DOSYA_ISLEM>();
            T_SIKAYET_AVUKAT = new HashSet<T_SIKAYET_AVUKAT>();
        }

        public long id { get; set; }

        [StringLength(20)]
        public string dosya_kod { get; set; }

        [StringLength(50)]
        public string adi_soyadi { get; set; }

        [StringLength(20)]
        public string cep_telefonu { get; set; }

        [StringLength(150)]
        public string e_posta_adresi { get; set; }

        public int? il_id { get; set; }

        public int? brans_id { get; set; }

        public int? iletisim_tercihi { get; set; }

        public string sikayet_aciklama { get; set; }

        [StringLength(50)]
        public string dosya1 { get; set; }

        [StringLength(50)]
        public string dosya2 { get; set; }

        [StringLength(50)]
        public string dosya3 { get; set; }

        public bool? dosya_kapat { get; set; }

        [StringLength(50)]
        public string dosya_kapat_neden { get; set; }

        [StringLength(10)]
        public string sil_kodu { get; set; }

        public int? sil_hata_sayisi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? islem_tarihi { get; set; }

        public int? durumu { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? sikayet_tarihi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_AVUKAT_DOSYA_ISLEM> T_AVUKAT_DOSYA_ISLEM { get; set; }

        public virtual T_BRANS T_BRANS { get; set; }

        public virtual T_SEHIR T_SEHIR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_SIKAYET_AVUKAT> T_SIKAYET_AVUKAT { get; set; }
    }
}
