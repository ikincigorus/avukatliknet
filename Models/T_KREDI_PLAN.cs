namespace avukatliknet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_KREDI_PLAN
    {
        public long id { get; set; }

        public int? kredi { get; set; }

        public decimal? fiyat { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? gecerlilik_baslangic { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? gecerlilik_bitis { get; set; }

        public bool? ap { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? kayit_tarihi { get; set; }
    }
}
