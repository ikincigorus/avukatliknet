﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace avukatliknet.Models
{
    public static class _mailing
    {
        public static readonly string link = ConfigurationManager.AppSettings["urllink"];

        public static void MailSender(string to, string subject, string body, bool bcc)
        {
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            NetworkCredential credentials = new NetworkCredential("avukatliknet2@gmail.com", "sxxd1984");
            client.UseDefaultCredentials = false;
            client.Credentials = credentials;

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("info@avukatlik.com", "Yönetim");
            msg.To.Add(new MailAddress(to));
            if (bcc == true)
            {
                msg.Bcc.Add("oguzhan1981@gmail.com");
                msg.Bcc.Add("imaden@gmail.com");
            }
            msg.Subject = subject;
            msg.BodyEncoding = UTF8Encoding.UTF8;
            msg.IsBodyHtml = true;
            msg.Body = Template1(string.Format(body));
            try
            {
                client.Send(msg);
            }
            catch (Exception ex)
            {
                string exep = ex.Message;
            }
        }

        public static string Template1(string body)
        {
            string tmp1 = @"<html xmlns='http://www.w3.org/1999/xhtml'>
                            <head>
                            <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
                            <meta name='viewport' content='width=device-width, initial-scale=1'>
                            <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                            <title>Avukatlik.net</title>
                            <style>

                                @import url('https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800,900&display=swap');
                                @import url('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');

                                .ReadMsgBody {
                                    width: 100%;
                                    background-color: #ffffff;
                                }

                                .ExternalClass {
                                    width: 100%;
                                    background-color: #ffffff;
                                }

                                    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
                                        line-height: 100%;
                                    }

                                html {
                                    width: 100%;
                                }

                                body {
                                    -webkit-text-size-adjust: none;
                                    -ms-text-size-adjust: none;
                                    margin: 0;
                                    padding: 0;
                                    background-color: #F7F7F7;
                                }

                                table {
                                    border-spacing: 0;
                                    table-layout: fixed;
                                    margin: 0 auto;
                                }

                                img {
                                    -ms-interpolation-mode: bicubic;
                                    display: block;
                                    width: auto;
                                    max-width: none;
                                    height: auto;
                                }

                                a[x-apple-data-detectors] {
                                    color: inherit !important;
                                    text-decoration: none !important;
                                    font-size: inherit !important;
                                    font-family: inherit !important;
                                    font-weight: inherit !important;
                                    line-height: inherit !important;
                                }

                                u + #body a {
                                    color: inherit;
                                    font-family: inherit;
                                    text-decoration: none;
                                    font-size: inherit;
                                    font-weight: inherit;
                                    line-height: inherit;
                                }

                                .appleLinks a {
                                    color: #c2c2c2 !important;
                                    text-decoration: none !important;
                                }

                                span.preheader {
                                    display: none !important;
                                }

                                @media only screen and (max-width: 699px) {
                                    table.hideMobile, tr.hideMobile, td.hideMobile, br.hideMobile {
                                        display: none !important;
                                    }

                                    table.row, div.row {
                                        width: 100% !important;
                                        max-width: 100% !important;
                                    }

                                    table.centerFloat, td.centerFloat, img.centerFloat {
                                        float: none !important;
                                        margin: 0 auto !important;
                                    }

                                    table.halfRow, div.halfRow {
                                        width: 50% !important;
                                        max-width: 50% !important;
                                    }

                                    td.imgResponsive img {
                                        width: 100% !important;
                                        max-width: 100% !important;
                                        height: auto !important;
                                        margin: auto;
                                    }

                                    td.menuLink a {
                                        display: block;
                                        border-top: 1px solid;
                                        padding-top: 20px;
                                    }

                                    td.centerText {
                                        text-align: center !important;
                                    }

                                    td.containerPadding {
                                        width: 100% !important;
                                        padding-left: 15px !important;
                                        padding-right: 15px !important;
                                    }

                                    td.spaceControl {
                                        height: 15px !important;
                                        font-size: 15px !important;
                                        line-height: 15px !important;
                                    }
                                }
                            </style>
                        </head>
                        <body marginwidth='0' marginheight='0' style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;' offset='0' topmargin='0' leftmargin='0'>
                            <table class='bodyBgColor' width='100%' border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#F7F7F7' style='width: 100%; max-width: 100%; position: relative; opacity: 1; top: 0px; left: 0px;'>
                                <tbody>
                                    <tr>
                                        <td align='center' valign='top'>
                                            <table class='row' width='700' border='0' align='center' cellpadding='0' cellspacing='0' style='width:700px;max-width:700px;'>
                                                <tbody>
                                                    <tr>
                                                        <td class='greyBgcolor' align='center' valign='top' bgcolor='#F7F7F7'>
                                                            <table class='row' width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='width:600px;max-width:600px;'>
                                                                <tbody>
                                                                    <tr>
                                                                        <td height='15' align='center' valign='top' style='font-size:15px;line-height:15px;'>&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height='15' align='center' valign='top' style='font-size:15px;line-height:15px;'>&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class='bodyBgColor' width='100%' border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#F7F7F7' style='width:100%;max-width:100%;'>
                                <tbody>
                                    <tr>
                                        <td align='center' valign='top'>
                                            <table class='row' width='700' border='0' align='center' cellpadding='0' cellspacing='0' style='width:700px;max-width:700px;'>
                                                <tbody>
                                                    <tr>
                                                        <td class='whiteBgcolor' align='center' valign='top' bgcolor='#FFFFFF'>
                                                            <table class='row' width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='width:600px;max-width:600px;'>
                                                                <tbody>
                                                                    <tr>
                                                                        <td height='30' align='center' valign='top' style='font-size:30px;line-height:30px;'>&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align='left' valign='top' style='font-size:0;padding:0'>
                                                                            <!--[if (gte mso 9)|(IE)]><table border='0' cellpadding='0' cellspacing='0'><tr><td valign='middle'><![endif]-->
                                                                            <div class='row' style='display:inline-block;max-width:130px;vertical-align:middle;width:100%'>
                                                                                <table class='row' border='0' align='left' cellpadding='0' cellspacing='0'>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align='center' valign='top'>
                                                                                                <a href='{0}/'>
                                                                                                    <img data-color='Logo' src='{0}/content/images/brand/logo.png' width='160' alt='' style='display:block;border:0;width:160px;'>
                                                                                                </a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <!--[if (gte mso 9)|(IE)]></td><td valign='middle'><![endif]-->
                                                                            <div class='row' style='display:inline-block;max-width:20px;vertical-align:middle;width:100%'>
                                                                                <table class='row' border='0' align='left' cellpadding='0' cellspacing='0'>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td height='30' align='center' valign='top' style='font-size:30px; line-height:30px;'>&nbsp;</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <!--[if (gte mso 9)|(IE)]></td><td valign='middle'><![endif]-->
                                                                            <div class='row' style='display:inline-block;max-width:450px;vertical-align:middle;width:100%'>
                                                                                <table class='row' border='0' align='right' cellpadding='0' cellspacing='0'>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td data-size='Menu Link' class='menuLink' align='center' valign='middle' style='font-family:Poppins, sans-serif; font-size:14px; font-weight:700; color:#4a4a4a; line-height:24px;'>
                                                                                                <a data-color='Menu Link' href='{0}/' style='text-decoration:none; color:#455A7A;'>Ana Sayfa</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                                                <a data-color='Menu Link' href='{0}/biz-kimiz' style='text-decoration:none; color:#455A7A;'>Biz Kimiz</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                                                <a data-color='Menu Link' href='{0}/ne-is-yapariz' style='text-decoration:none; color:#455A7A;'>Ne İş Yaparız</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                                                <a data-color='Menu Link' href='{0}/iletisim' style='text-decoration:none; color:#455A7A;'>İletişim</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height='30' align='center' valign='top' style='font-size:30px;line-height:30px;'>&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class='bodyBgColor' width='100%' border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#F7F7F7' style='width:100%;max-width:100%;'>
                                <tbody>
                                    <tr>
                                        <td align='center' valign='top'>
                                            <table class='row' width='700' border='0' align='center' cellpadding='0' cellspacing='0' style='width:700px;max-width:700px;'>
                                                <tbody>
                                                    <tr>
                                                        <td class='blackBgcolor' align='center' valign='top' bgcolor='#455A7A'>
                                                            <table class='row' width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='width:600px;max-width:600px;'>
                                                                <tbody>
                                                                    <tr>
                                                                        <td height='5' align='center' valign='top' style='font-size:10px;line-height:5px;'>&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class='bodyBgColor' width='100%' border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#F7F7F7' style='width:100%;max-width:100%;'>
                                <tbody>
                                    <tr>
                                        <td align='center' valign='top'>
                                            <table class='row' width='700' border='0' align='center' cellpadding='0' cellspacing='0' style='width:700px;max-width:700px;'>
                                                <tbody>
                                                    <tr>
                                                        <td class='whiteBgcolor' align='center' valign='top' bgcolor='#FFFFFF'>
                                                            <table class='row' width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='width:600px;max-width:600px;'>
                                                                <tbody>
                                                                    <tr>
                                                                        <td height='30' align='center' valign='top' style='font-size:30px;line-height:30px;'>&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class='containerPadding' align='left' valign='top' style='font-size:0;padding:0'>
                                                                            <!--[if (gte mso 9)|(IE)]><table border='0' cellpadding='0' cellspacing='0'><tr><td valign='middle'><![endif]-->
                                                                            <div class='row' style='display:inline-block;max-width:600px;vertical-align:middle;width:100%'>
                                                                                <table class='row' width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='width:100%;max-width:600px;'>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class='Headlines centerText' align='left' valign='middle' style='font-family:Poppins,Arial,Helvetica,sans-serif;color:#000000;font-size:12px;line-height:15px;font-weight:500;letter-spacing:0px;padding:0;padding-bottom:20px;'>";
            string tmp2 = @"</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height='30' align='center' valign='top' style='font-size:30px;line-height:30px;'>&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class='bodyBgColor' width='100%' border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#F7F7F7' style='width:100%;max-width:100%;'>
                                <tbody>
                                    <tr>
                                        <td align='center' valign='top'>
                                            <table class='row' width='700' border='0' align='center' cellpadding='0' cellspacing='0' style='width:700px;max-width:700px;'>
                                                <tbody>
                                                    <tr>
                                                        <td class='blackBgcolor' align='center' valign='top' bgcolor='#2D3B50'>
                                                            <table class='row' width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='width:600px;max-width:600px;'>
                                                                <tbody>
                                                                    <tr>
                                                                        <td height='30' align='center' valign='top' style='font-size:30px;line-height:30px;'>&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align='center' valign='top' style='font-size:0;padding:0'>
                                                                            <!--[if (gte mso 9)|(IE)]><table border='0' cellpadding='0' cellspacing='0'><tr><td valign='middle'><![endif]-->
                                                                            <div class='row' style='display:inline-block;max-width:290px;vertical-align:middle;width:100%'>
                                                                                <table class='row' align='left' border='0' cellpadding='0' cellspacing='0'>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class='footerDescription centerText' align='left' valign='middle' style='font-family:Open Sans,Arial,helvetica,sans-serif;color:#d5d5d5;font-size:14px;line-height:22px;font-weight:400;letter-spacing:0px;'>
                                                                                                © 2020 Avukatlik.net Tüm hakları saklıdır.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <!--[if (gte mso 9)|(IE)]></td><td valign='top'><![endif]-->
                                                                            <div class='row' style='display:inline-block;max-width:20px;vertical-align:top;width:100%'>
                                                                                <table class='row' border='0' align='left' cellpadding='0' cellspacing='0'>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td height='20' align='center' valign='top' style='font-size:20px;line-height:20px;'>&nbsp;</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height='30' align='center' valign='top' style='font-size:30px;line-height:30px;'>&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class='parentOfBg'></div>
                        </body>
                        </html>".Replace("{0}" ,link);

            return tmp1 + body + tmp2;
        }
    }
}