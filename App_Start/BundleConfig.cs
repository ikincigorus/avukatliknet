﻿using System.Web;
using System.Web.Optimization;

namespace avukatliknet
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
           
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/plugins/bootstrap/css/bootstrap.min.css",
                      "~/Content/css/style.css",
                      "~/Content/css/software.css",
                      "~/Content/css/software.css",
                      "~/Content/fonts/fonts/font-awesome.min.css",
                      "~/Content/plugins/Horizontal2/Horizontal-menu/dropdown-effects/fade-down.css",
                      "~/Content/plugins/Horizontal2/Horizontal-menu/horizontal.css",
                      "~/Content/plugins/Horizontal2/Horizontal-menu/color-skins/color.css",
                      "~/Content/plugins/select2/select2.min.css",
                      "~/Content/plugins/cookie/cookie.css",
                      "~/Content/plugins/owl-carousel/owl.carousel.css",
                      "~/Content/plugins/scroll-bar/jquery.mCustomScrollbar.css",
                      "~/Content/plugins/iconfonts/plugin.css",
                      "~/Content/plugins/iconfonts/icons.css",
                      "~/Content/webslidemenu/color-skins/color1.css"));


            bundles.Add(new ScriptBundle("~/Content/js").Include(
                       "~/Content/plugins/bootstrap/js/popper.min.js",
                       "~/Content/plugins/bootstrap/js/bootstrap.min.js",
                       "~/Content/js/vendors/jquery.sparkline.min.js",
                       "~/Content/js/vendors/circle-progress.min.js",
                       "~/Content/plugins/rating/jquery.rating-stars.js",
                       "~/Content/plugins/counters/counterup.min.js",
                       "~/Content/plugins/counters/waypoints.min.js",
                       "~/Content/plugins/counters/numeric-counter.js",
                       "~/Content/plugins/owl-carousel/owl.carousel.js",
                       "~/Content/plugins/Horizontal2/Horizontal-menu/horizontal.js",
                       "~/Content/js/jquery.touchSwipe.min.js",
                       "~/Content/plugins/select2/select2.full.min.js",
                       "~/Content/js/select2.js",
                       "~/Content/plugins/cookie/jquery.ihavecookies.js",
                       "~/Content/plugins/cookie/cookie.js",
                       "~/Content/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js",
                       "~/Content/js/sticky.js",
                       "~/Content/js/carousel.js",
                       "~/Content/js/swipe.js",
                       "~/Content/js/custom.js"));


        }
    }
}
