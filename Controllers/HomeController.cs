﻿using avukatliknet.Models;
using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace avukatliknet.Controllers
{
    public class HomeController : Controller
    {
        EntityModel db = new EntityModel();
        _site site = new _site();
        public static readonly string link = ConfigurationManager.AppSettings["urllink"];
        public static readonly string onlinelink = ConfigurationManager.AppSettings["onlinelink"];

        [ErrorLog]
        public ActionResult Index()
        {
            site.M_BRANSS = db.T_BRANS.ToList();
            site.M_SLIDER_AVUKATS = db.T_SLIDER_AVUKAT.Where(t => DateTime.Today >= t.baslama_tarihi  && DateTime.Today <= t.bitis_tarihi).ToList();
            site.AvukatSayi = db.T_AVUKAT.Where(t => t.durumu == 0).Count();
            site.DosyaSayi = db.T_SIKAYET.Count();
            site.MusteriSayi = db.T_SIKAYET.GroupBy(t => t.e_posta_adresi).Count();
            return View(site);
        }

        [ErrorLog]
        public ActionResult Biz_Kimiz()
        {
            site.M_BRANSS = db.T_BRANS.ToList();

            return View(site);
        }

        [ErrorLog]
        public ActionResult Ne_Is_Yapariz()
        {
            site.M_BRANSS = db.T_BRANS.ToList();

            return View(site);
        }

        [ErrorLog]
        public ActionResult Kullanim_Kosullari()
        {
            site.M_BRANSS = db.T_BRANS.ToList();

            return View(site);
        }

        [ErrorLog]
        public ActionResult KVKK_ve_Gizlilik_Politikasi()
        {
            site.M_BRANSS = db.T_BRANS.ToList();

            return View(site);
        }

        [ErrorLog]
        public ActionResult Iletisim()
        {
            site.M_BRANSS = db.T_BRANS.ToList();
            CreateCaptchaCode();
            return View(site);
        }

        [ErrorLog]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Iletisim(string adi_soyadi, string e_posta_adresi, string mesajiniz, string captcha)
        {
            if (Session["CaptchaCode"].ToString() == captcha)
            {
                site.ErrorStr = "<div class='text-red mb-5 font-weight-bold'>Lütfen güvenlik kodunu doğru yazınız.</div>";
                site.M_BRANSS = db.T_BRANS.ToList();
                site.M_SEHIRS = db.T_SEHIR.ToList();
                CreateCaptchaCode();
                return View(site);
            }



            return Redirect("/bilgilendirme?islem=iletisim-kayit");
        }

        [ErrorLog]
        public ActionResult Avukata_Sor(string dava_konusu)
        {
            site.M_BRANSS = db.T_BRANS.ToList();
            site.M_SEHIRS = db.T_SEHIR.ToList();
            site.BransStr = dava_konusu;
            CreateCaptchaCode();
            return View(site);
        }

        [ErrorLog]
        [ValidateInput(false)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Avukata_Sor(T_SIKAYET sikayets, string captcha, HttpPostedFileBase[] dosya)
        {
            if (Session["CaptchaCode"] == null || Session["CaptchaCode"].ToString() != captcha)
            {
                site.ErrorStr = "<div class='text-red mb-5 font-weight-bold'>Lütfen güvenlik kodunu doğru yazınız.</div>";
                site.M_BRANSS = db.T_BRANS.ToList();
                site.M_SEHIRS = db.T_SEHIR.ToList();
                CreateCaptchaCode();
                return View(site);
            }

            string pnr = _common.PnrGenerate();
            string strfile = "";

            T_SIKAYET sikayet = new T_SIKAYET();
            sikayet.dosya_kod = pnr;
            sikayet.adi_soyadi = Sanitizer.GetSafeHtmlFragment(sikayets.adi_soyadi);
            sikayet.brans_id = sikayets.brans_id;
            sikayet.cep_telefonu = Sanitizer.GetSafeHtmlFragment(sikayets.cep_telefonu);
            sikayet.e_posta_adresi = Sanitizer.GetSafeHtmlFragment(sikayets.e_posta_adresi);
            sikayet.iletisim_tercihi = sikayets.iletisim_tercihi;
            sikayet.il_id = sikayets.il_id;
            sikayet.sikayet_aciklama = Sanitizer.GetSafeHtmlFragment(sikayets.sikayet_aciklama);
            sikayet.durumu = 0;
            if (dosya.Length > 0 && dosya[0] != null)
            {
                Directory.CreateDirectory(Server.MapPath("~/Files/" + pnr));

                for (int i = 0; i < dosya.Length; i++)
                {
                    if (i > 2)
                        break;

                    dosya[i].SaveAs(Server.MapPath("~/Files/" + pnr + "/" + Path.GetFileName(dosya[i].FileName)));
                    strfile = dosya[i].FileName;
                    if (i == 0)
                        sikayet.dosya1 = strfile;
                    else if (i == 1)
                        sikayet.dosya2 = strfile;
                    else if (i == 2)
                        sikayet.dosya3 = strfile;
                }
            }
            sikayet.sikayet_tarihi = DateTime.Now;
            db.T_SIKAYET.Add(sikayet);
            db.SaveChanges();

            //Dosya gönderene mail
            _mailing.MailSender(sikayets.e_posta_adresi, "Avukatlik.net - Bilgilendirme", "Merhaba, <br /><br /> Dosyanız sistemimizdeki avukatlara gönderildi. Dosya takip numaranız : <b>" + pnr + "</b> .Dosyanızı takibini (Teklif ve Dosya bilgilerinizi) <a href='" + link + "/dosya-bul?dosya_kod=" + pnr + "&e_posta_adresi=" + sikayets.e_posta_adresi + "' target='_blank'>buradan</a> kontrol edebilirsiniz.<br /><br />İyi günler dileriz.", true);

            //Avukata mail
            List<T_AVUKAT> avukat = db.T_AVUKAT.Where(t => t.T_AVUKAT_BRANS.Any(b => b.brans_id == sikayets.brans_id) && t.T_AVUKAT_SEHIR.Any(s => s.il_id == sikayets.il_id)).ToList();

            foreach (var item in avukat)
            {
                _mailing.MailSender(item.e_posta_adresi, "Avukatlik.net - Yeni dosya", "Merhaba, <br /><br /> Kriterlerinize uygun yeni bir dosya geldi. İlgileniyorsanız <a href='" + onlinelink + "' target='_blank'>sisteme giriş</a> yaparak ilgili dosyaya teklif verebilirsiniz.<br /><br />İyi günler dileriz.", true);
            }

            return Redirect("/bilgilendirme/" + pnr + "?islem=dosya-kayit");
        }

        [ErrorLog]
        public ActionResult Avukat_Bilgilendirme()
        {
            return View(site);
        }

        [ErrorLog]
        public ActionResult Avukat_Kayit(string id)
        {
            site.M_BRANSS = db.T_BRANS.ToList();
            site.M_SEHIRS = db.T_SEHIR.ToList();
            site.ParamDataTable = _common.ParamaterList((List<T_PARAMS>)ParamKey("bizi_nereden_duydunuz", ""));
            site.KrediParams = (string)ParamKey("kredi_limit", "kredi_limit");
            
            T_AVUKAT avukat = db.T_AVUKAT.Where(t => t.avukat_code == id).FirstOrDefault();
            if (avukat != null)
            { 
                site.TavsiyeAdSoyad = avukat.adi_soyadi;
                site.TavsiyeRefKod = id;
            }
            if (Convert.ToInt16(_common.ParamKey("avukat_brans_secim", "avukat_brans_secim")) > 0)
                site.BransLimit = "En fazla " + _common.ParamKey("avukat_brans_secim", "avukat_brans_secim") + " branş ekleyebilirsiniz.";
            else
                site.BransLimit = "Birden çok branş ekleyebilirsiniz.";

            if (Convert.ToInt16(_common.ParamKey("avukat_sehir_secim", "avukat_sehir_secim")) > 0)
                site.SehirLimit = "En fazla " + _common.ParamKey("avukat_sehir_secim", "avukat_sehir_secim") + " şehir ekleyebilirsiniz.";
            else
                site.SehirLimit = "Birden çok şehir ekleyebilirsiniz.";

            CreateCaptchaCode();
            return View(site);
        }

        [ErrorLog]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Avukat_Kayit(T_AVUKAT avukats, string captcha, int[] brans_id, int[] ils_id, string id)
        {
            site.ErrorStr = "";

            int cep_telefonu_cont = db.T_AVUKAT.Where(t => t.cep_telefonu == avukats.cep_telefonu).Count();
            if (cep_telefonu_cont > 0)
                site.ErrorStr += "<div class='text-red mb-1'>Cep telefonu sistemde mevcut. Lütfen başka bir telefon numarası yazınız.</div>";

            int e_posta_cont = db.T_AVUKAT.Where(t => t.e_posta_adresi == avukats.e_posta_adresi).Count();
            if (e_posta_cont > 0)
                site.ErrorStr += "<div class='text-red mb-1'>E-posta adresi sistemde mevcut. Lütfen başka bir e-posta adresi yazınız.</div>";

            if (Convert.ToInt16(_common.ParamKey("avukat_brans_secim", "avukat_brans_secim")) > 0 && brans_id.Length > Convert.ToInt16(_common.ParamKey("avukat_brans_secim", "avukat_brans_secim")))
                site.ErrorStr += "<div class='text-red mb-1'>En fazla " + Convert.ToInt16(_common.ParamKey("avukat_brans_secim", "avukat_brans_secim")) + " branş ekleyebilirsiniz.</div>";

            if (Convert.ToInt16(_common.ParamKey("avukat_sehir_secim", "avukat_sehir_secim")) > 0 && ils_id.Length > Convert.ToInt16(_common.ParamKey("avukat_sehir_secim", "avukat_sehir_secim")))
                site.ErrorStr += "<div class='text-red mb-1'>En fazla " + Convert.ToInt16(_common.ParamKey("avukat_sehir_secim", "avukat_sehir_secim")) + " şehir ekleyebilirsiniz.</div>";

            if (Session["CaptchaCode"] == null || Session["CaptchaCode"].ToString() != captcha)
                site.ErrorStr += "<div class='text-red mb-1'>Lütfen güvenlik kodunu doğru yazınız.</div>";

            if (site.ErrorStr != "")
            {
                site.M_BRANSS = db.T_BRANS.ToList();
                site.M_SEHIRS = db.T_SEHIR.ToList();
                site.ParamDataTable = _common.ParamaterList((List<T_PARAMS>)ParamKey("bizi_nereden_duydunuz", ""));
                site.KrediParams = (string)ParamKey("kredi_limit", "kredi_limit");

                T_AVUKAT avukat_tavsiye = db.T_AVUKAT.Where(t => t.avukat_code == id).FirstOrDefault();
                if (avukat_tavsiye != null)
                {
                    site.TavsiyeAdSoyad = avukat_tavsiye.adi_soyadi;
                    site.TavsiyeRefKod = id;
                }
                if (Convert.ToInt16(_common.ParamKey("avukat_brans_secim", "avukat_brans_secim")) > 0)
                    site.BransLimit = "En fazla " + _common.ParamKey("avukat_brans_secim", "avukat_brans_secim") + " branş ekleyebilirsiniz.";
                else
                    site.BransLimit = "Birden çok branş ekleyebilirsiniz.";

                if (Convert.ToInt16(_common.ParamKey("avukat_sehir_secim", "avukat_sehir_secim")) > 0)
                    site.SehirLimit = "En fazla " + _common.ParamKey("avukat_sehir_secim", "avukat_sehir_secim") + " şehir ekleyebilirsiniz.";
                else
                    site.SehirLimit = "Birden çok şehir ekleyebilirsiniz.";

                CreateCaptchaCode();
                return View(site);
            }

            int kredi_limit = Convert.ToInt32(ParamKey("kredi_limit", "kredi_limit"));
            T_AVUKAT avukat_oneren_dogrumu = db.T_AVUKAT.Where(t => t.avukat_code == id).FirstOrDefault();

            T_AVUKAT avukat = new T_AVUKAT();
            avukat.avukat_code = _common.GetGuid();
            avukat.adi_soyadi = avukats.adi_soyadi;
            avukat.cep_telefonu = avukats.cep_telefonu;
            avukat.e_posta_adresi = avukats.e_posta_adresi;
            avukat.adresi = avukats.adresi;
            avukat.il_id = avukats.il_id;
            avukat.bagli_olunan_baro = avukats.bagli_olunan_baro;
            string password = _common.CreatePassword(8);
            string passwordhash = _common.GetHashString(password);
            avukat.sifre = passwordhash;
            avukat.kredi = kredi_limit;
            avukat.bizi_nereden_duydunuz = avukats.bizi_nereden_duydunuz;
            if (avukat_oneren_dogrumu != null)
            {
                //Referansla geldiyse referans olan kişinin kodu
                avukat.avukat_oneren = id;
            }
            avukat.aktiflik_puani = 0;
            avukat.uyelik_tarihi = DateTime.Now;
            avukat.durumu = 0;
            avukat.goruntuleme_sayisi = 0;
            avukat.sehir_ekleme_sayisi = 0;
            avukat.brans_ekleme_sayisi = 0;
            avukat.yonetici = 0;

            db.T_AVUKAT.Add(avukat);

            foreach (var item in brans_id)
            {
                T_AVUKAT_BRANS av_brans = new T_AVUKAT_BRANS();
                av_brans.avukat_id = avukat.id;
                av_brans.brans_id = item;
                db.T_AVUKAT_BRANS.Add(av_brans);
            }

            foreach (var item in ils_id)
            {
                T_AVUKAT_SEHIR av_sehir = new T_AVUKAT_SEHIR();
                av_sehir.avukat_id = avukat.id;
                av_sehir.il_id = item;
                db.T_AVUKAT_SEHIR.Add(av_sehir);
            }

            T_AVUKAT_KREDI_LOG kredi_log = new T_AVUKAT_KREDI_LOG();
            kredi_log.avukat_id = avukat.id;
            if (avukat_oneren_dogrumu != null)
            {
                //Referansla geldiyse kredi limitine parametredeki kadar hediye
                kredi_log.kredi_limit = kredi_limit + Convert.ToInt32(ParamKey("kredi_limit_tavsiye_edilen", "kredi_limit_tavsiye_edilen"));
                kredi_log.aciklama = "İlk kayıt işlemi tutarınız olan " + kredi_limit + " (+" + ParamKey("kredi_limit_tavsiye_edilen", "kredi_limit_tavsiye_edilen") + " hediye kredi) kredi hesabınıza işlendi.";
            }
            else
            {
                kredi_log.kredi_limit = kredi_limit;
                kredi_log.aciklama = "İlk kayıt işlemi tutarınız olan " + kredi_limit + " kredi hesabınıza işlendi.";
            }
            kredi_log.islem = "+";
            kredi_log.islem_tarihi = DateTime.Now;
            db.T_AVUKAT_KREDI_LOG.Add(kredi_log);

            if (avukat_oneren_dogrumu != null)
            {   //Öneren avukatın krediyi ve aktiflik puanını ekle
                avukat_oneren_dogrumu.kredi += Convert.ToInt32(ParamKey("kredi_limit_tavsiye_eden", "kredi_limit_tavsiye_eden"));
                avukat_oneren_dogrumu.aktiflik_puani += Convert.ToInt32(ParamKey("avukat_aktiflik_tavsiye_et", "avukat_aktiflik_tavsiye_et"));

                //Öneren avukatın kredi log kaydı yap
                T_AVUKAT_KREDI_LOG avukat_oneren_kredi_log = new T_AVUKAT_KREDI_LOG();
                avukat_oneren_kredi_log.avukat_id = avukat_oneren_dogrumu.id;
                avukat_oneren_kredi_log.kredi_limit = Convert.ToInt32(ParamKey("kredi_limit_tavsiye_eden", "kredi_limit_tavsiye_eden")); ;
                avukat_oneren_kredi_log.aciklama = "Arkadaşınız " + avukats.adi_soyadi + " sizin referansınızla kayıt olarak size " + Convert.ToInt32(ParamKey("kredi_limit_tavsiye_eden", "kredi_limit_tavsiye_eden")) + " kredi kazandırdı.";
                avukat_oneren_kredi_log.islem = "+";
                avukat_oneren_kredi_log.islem_tarihi = DateTime.Now;
                db.T_AVUKAT_KREDI_LOG.Add(avukat_oneren_kredi_log);
            }

            db.SaveChanges();

            //Kayıt olan Avukata mail
            _mailing.MailSender(avukat.e_posta_adresi, "Avukatlik.net - Kayıt Bilgilendirme", "Merhaba, kaydınız başarıyla oluşturuldu. <a href='" + onlinelink + "' class=\"text-blue\" target=\"_blank\">Online işlemler ekranına</a> giriş bilgileriniz, <br /><br /><b>E-posta : </b>" + avukat.e_posta_adresi + "<br /><b>Şifreniz : </b>" + password + "<br /><br />İyi günler dileriz.", true);

            //Öneren avukatın kredi yüklendi haberini verelim
            if (avukat_oneren_dogrumu != null)
            {
                _mailing.MailSender(avukat_oneren_dogrumu.e_posta_adresi, "Avukatlik.net - Bilgilendirme", "Arkadaşınız " + avukat.adi_soyadi + " sizin referansınızla kayıt olarak size " + Convert.ToInt32(ParamKey("kredi_limit_tavsiye_eden", "kredi_limit_tavsiye_eden")) + " kredi kazandırdı.<br /><br />İyi günler dileriz.", true);
            }

            return Redirect("/bilgilendirme?islem=avukat-kayit");
        }

        [ErrorLog]
        public ActionResult Bilgilendirme(string id, string islem)
        {
            try
            {
                if (islem == "avukat-kayit")
                {
                    site.SuccessStr = "Merhaba, kaydınız başarıyla oluşturuldu. <a href=\"https://online.avukatlik.net\" class=\"text-blue\" target=\"_blank\">Online işlemler ekranına</a> giriş bilgileriniz, e-posta adresinize gönderildi. <br /><br />İyi günler dileriz.";
                }
                else if (islem == "dosya-kayit")
                {
                    T_SIKAYET sikayet = db.T_SIKAYET.Where(t => t.dosya_kod == id).FirstOrDefault();
                    long inceleyecek_avukat = db.T_AVUKAT.Where(t => t.T_AVUKAT_BRANS.Any(b => b.brans_id == sikayet.brans_id) && t.T_AVUKAT_SEHIR.Any(s => s.il_id == sikayet.il_id)).Count();
                    site.SuccessStr = "Merhaba, kaydınız başarıyla oluşturuldu ve dosya numaranız (<strong>" + id + "</strong>) e-posta adresinize gönderildi. E-posta adresinize gönderdiğimiz linkten dosyanıza ulaşabilir ve tekliflere bakabilirsiniz. Teklifler geldiğinde mail ile bildirilecektir. <br /><br /><b>Not :</b> Bu dosya incelenmesi için <b>" + inceleyecek_avukat + "</b> avukata gönderilmiştir.<br /><br />İyi günler dileriz.";
                }
                else if (islem == "iletisim-kayit")
                {
                    site.SuccessStr = "Merhaba, iletmiş olduğunuz bilgiler tarafımıza ulaşmıştır. Size en yakın sürede geri dönüş yapılacaktır. <br /><br />İyi günler dileriz.";
                }
                else if (islem == "dosya-kapat")
                {
                    site.SuccessStr = "Merhaba, dosyanız kapatılmıiştır. <br /><br />İyi günler dileriz.";
                }
                else if (islem == "dosya-sil")
                {
                    site.SuccessStr = "Merhaba, dosya silme işleminizi onaylamak için sistem de kayıtlı olan e-posta adrenize mesaj gönderdik. Lütfen mailinizi onaylayınız.<br /><br />İyi günler dileriz.";
                }
                else if (islem == "dosya-silindi")
                {
                    site.SuccessStr = "Merhaba, dosyanız sistemden silinmiştir. <br /><br />İyi günler dileriz.";
                }
                else
                {
                    site.SuccessStr = "Hatalı işlem.";
                }
            }
            catch
            {
                site.SuccessStr = "Hatalı işlem.";
            }

            return View(site);
        }

        [ErrorLog]
        public ActionResult Dosya_Bul(string dosya_kod, string e_posta_adresi)
        {
            site.DosyaKod = dosya_kod;
            site.EPostaAdresi = e_posta_adresi;
            CreateCaptchaCode();
            site.DosyaGoster = 0;
            return View(site);
        }

        [ErrorLog]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Dosya_Bul(string dosya_kod, string e_posta_adresi, string captcha)
        {
            site.ErrorStr = "";
            site.DosyaGoster = 0;
            T_SIKAYET sikayet = db.T_SIKAYET.Where(t => t.dosya_kod == dosya_kod && t.e_posta_adresi == e_posta_adresi).FirstOrDefault();
            if (sikayet == null)
                site.ErrorStr += "<div class='text-red mb-5 font-weight-bold'>Dosyanız sistemde mevcut değil.</div>";
            else
            {
                if (Session["CaptchaCode"] == null || Session["CaptchaCode"].ToString() != captcha)
                    site.ErrorStr += "<div class='text-red mb-5 font-weight-bold'>Lütfen güvenlik kodunu doğru yazınız.</div>";
            }

            if (site.ErrorStr != "")
            {
                CreateCaptchaCode();
                return View(site);
            }

            site.DurumStr = (string)ParamKey("sikayet_durum", sikayet.durumu.ToString());
            site.DosyaGoster = 1;
            site.M_SIKAYET = sikayet;

            return View(site);
        }

        [ErrorLog]
        public ActionResult Dosya_Sil(string dosya, string eposta, string silkod, string islem)
        {
            site.ErrorStr = "";
            T_SIKAYET sikayet = db.T_SIKAYET.Where(t => t.dosya_kod == dosya && t.e_posta_adresi == eposta && t.sil_kodu == silkod && t.durumu == 95).FirstOrDefault();
            if (sikayet == null)
                site.ErrorStr += "<div class='text-red mb-5 font-weight-bold'>Dosyanız sistemde mevcut değil.</div>";

            if (site.ErrorStr != "")
            {
                return Redirect("/bilgilendirme");
            }

            if (islem == "dosyasil")
            {
                sikayet.adi_soyadi = "";
                sikayet.cep_telefonu = "";
                sikayet.e_posta_adresi = "";
                sikayet.sikayet_aciklama = "";
                sikayet.islem_tarihi = DateTime.Now;
                sikayet.durumu = 96; //Durum 96 ise silindi.
                db.SaveChanges();
                //_mailing.MailSender(sikayet.e_posta_adresi, "Avukatlik.net - Dosya Sil Bilgilendirme", "Merhaba, dosyanız silinecek. Dosya silme işlemi için <a href='" + onlinelink + "/dosya-islem/dosya=" + dosya_kod + "&eposta=" + sikayet.e_posta_adresi + "&islem=dosyasil' class=\"text-blue\" target=\"_blank\">buraya</a> tıklayınız.", true);
                return Redirect("/bilgilendirme?islem=dosya-silindi");
            }

            return Redirect("/bilgilendirme");
        }

        [ErrorLog]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Dosya_Islem(string dosya_kod, string dosya_anket, string buton)
        {
            site.ErrorStr = "";
            T_SIKAYET sikayet = db.T_SIKAYET.Where(t => t.dosya_kod == dosya_kod).FirstOrDefault();
            if (sikayet == null)
                site.ErrorStr += "<div class='text-red mb-5 font-weight-bold'>Dosyanız sistemde mevcut değil.</div>";

            if (site.ErrorStr != "")
            {
                return View(site);
            }

            if (buton == "dosyakapat")
            {
                sikayet.durumu = 90; //Durum 90 ise kapatıldı.
                sikayet.dosya_kapat = true;
                sikayet.dosya_kapat_neden = dosya_anket;
                sikayet.islem_tarihi = DateTime.Now;
                db.SaveChanges();
                return Redirect("/bilgilendirme?islem=dosya-kapat");
            }
            else if (buton == "dosyasil")
            {
                string deletePnr = _common.CreateNumber(5);
                sikayet.sil_kodu = deletePnr;
                sikayet.durumu = 95; //Durum 95 ise silinecek.
                db.SaveChanges();
                _mailing.MailSender(sikayet.e_posta_adresi, "Avukatlik.net - Dosya Sil Bilgilendirme", "Merhaba, dosyanız silinecek. Dosya silme işlemi için <a href='" + onlinelink + "/dosya-islem/dosya=" + dosya_kod + "&eposta=" + sikayet.e_posta_adresi + "&silkod=" + deletePnr + "&islem=dosyasil' class=\"text-blue\" target=\"_blank\">buraya</a> tıklayınız.", true);
                return Redirect("/bilgilendirme?islem=dosya-sil");
            }

            return Redirect("/bilgilendirme");
        }

        public ActionResult Avukat_Detay(string id)
        {
            site.M_AVUKAT = db.T_AVUKAT.Where(t => t.avukat_code == id).FirstOrDefault();
            return View(site);
        }

        public ActionResult Email_Goruntule()
        {
            return View();
        }

        [ErrorLog]
        public ActionResult Hata()
        {
            return View(site);
        }

        [ErrorLog]
        private object ParamKey(string grup, string key)
        {
            if (key == "")
                return db.T_PARAMS.Where(t => t.grup == grup).ToList();
            else
                return db.T_PARAMS.Where(t => t.grup == grup && t.keys == key).FirstOrDefault().value;
        }

        [ErrorLog]
        public FileContentResult GetCaptchaCode()
        {
            return GetCaptcha2();
        }

        [ErrorLog]
        private FileContentResult GetCaptcha2()
        {
            _captchaImage ci = new _captchaImage(Session["CaptchaCode"].ToString(), 200, 41, "Arial");
            MemoryStream ms = new MemoryStream();
            ci.Image.Save(ms, ImageFormat.Jpeg);
            ci.Dispose();
            byte[] imageBytes = ms.ToArray();
            FileContentResult img = File(ms.ToArray(), "image/gif");
            //string base64String = Convert.ToBase64String(imageBytes);
            return File(img.FileContents, img.ContentType);
        }

        [ErrorLog]
        private string GetCaptcha()
        {
            _captchaImage ci = new _captchaImage(Session["CaptchaCode"].ToString(), 200, 41, "Arial");
            MemoryStream ms = new MemoryStream();
            ci.Image.Save(ms, ImageFormat.Jpeg);
            ci.Dispose();
            byte[] imageBytes = ms.ToArray();
            //FileContentResult img = File(ms.ToArray(), "image/gif");
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String; //File(img.FileContents, img.ContentType);
        }

        [ErrorLog]
        public FileContentResult CreateCaptchaCode()
        {
            return CreateCaptcha(GenerateRandomCode());
        }

        [ErrorLog]
        private FileContentResult CreateCaptcha(string generate)
        {
            Session["CaptchaCode"] = generate;
            _captchaImage ci = new _captchaImage(Session["CaptchaCode"].ToString(), 200, 41, "Arial");
            MemoryStream ms = new MemoryStream();
            ci.Image.Save(ms, ImageFormat.Jpeg);
            ci.Dispose();
            FileContentResult img = File(ms.ToArray(), "image/gif");
            return File(img.FileContents, img.ContentType);
        }

        [ErrorLog]
        private string GenerateRandomCode()
        {
            Random random = new Random();
            string s = "";
            for (int i = 0; i < 6; i++)
                s = String.Concat(s, random.Next(10).ToString());
            return s;
        }
    }
}